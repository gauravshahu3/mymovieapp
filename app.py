import db
from flask import Flask, jsonify, request

app = Flask(__name__)


def get_role(request_data, methods=["GET"]):
    user_data = db.db.user_data.find_one(
        {"user_id": request_data["user"]["user_id"]}, {"_id": 0}
    )

    return user_data["role"]


"""This API will work for complete and incomelete string both. 
You can search movies by movie name directly."""


@app.route("/search_by_movie_name", methods=["GET"])
def search_by_movie_name():

    request_data = request.get_json()
    print("request_data", request_data)
    movie_details = list(
        db.db.imdb_movie_data.find(
            {"name": {"$regex": request_data["name"], "$options": "i"}}, {"_id": 0}
        )
    )
    return jsonify({"success": True, "data": movie_details}), 200


"""This API will work for complete and incomelete string both. 
You can search movies by director name."""


@app.route("/search_by_director_name", methods=["GET"])
def search_by_director_name():
    request_data = request.get_json()
    print("request_data", request_data)
    movie_details = list(
        db.db.imdb_movie_data.find(
            {"director": {"$regex": request_data["director"], "$options": "i"}},
            {"_id": 0},
        )
    )
    return jsonify({"success": True, "data": movie_details}), 200


"""This API will give search based on genre. """


@app.route("/search_by_genre", methods=["GET"])
def search_by_genre():
    request_data = request.get_json()
    print("request_data", request_data)
    movie_details = list(
        db.db.imdb_movie_data.find(
            {"genre": {"$all": request_data["genre"]}}, {"_id": 0}
        )
    )
    return jsonify({"success": True, "data": movie_details}), 200


"""This API will give the search feature based on IMDB score. 
The API will return only list of movies who will 
have greater IMDB score than the user entered value."""


@app.route("/search_for_greater_than_imdb_score", methods=["GET"])
def search_for_greater_than_imdb_score():
    request_data = request.get_json()
    print("request_data", request_data)
    movie_details = list(
        db.db.imdb_movie_data.find(
            {"imdb_score": {"$gt": request_data["imdb_score"]}}, {"_id": 0}
        )
    )
    return jsonify({"success": True, "data": movie_details}), 200


"""This is the filter API which will allow user to set IMDB value and genre."""


@app.route("/filter_search_imdb_value_and_genre", methods=["GET"])
def filter_search_imdb_value_and_genre():
    request_data = request.get_json()
    movie_details = list(
        db.db.imdb_movie_data.find(
            {
                "$and": [
                    {"imdb_score": {"$gt": request_data["imdb_score"]}},
                    {"genre": {"$all": request_data["genre"]}},
                ]
            },
            {"_id": 0},
        )
    )
    return jsonify({"success": True, "data": movie_details}), 200


"""This API will help admin in adding the movie data"""


@app.route("/add_data", methods=["GET"])
def add_data():
    request_data = request.get_json()
    try:
        if get_role(request_data) == "admin":
            db.db.imdb_movie_data.insert_one(
                {
                    "99popularity": float(request_data["99popularity"]),
                    "director": request_data["director"],
                    "genre": request_data["genre"],
                    "imdb_score": float(request_data["imdb_score"]),
                    "name": request_data["name"],
                }
            )
            return "Data Added Successfully."
    except TypeError:
        return (
            jsonify(
                {
                    "success": True,
                    "data": "You don't have Access to perform this action.",
                }
            ),
            401,
        )


"""This API will help admin in deleting the movie data"""


@app.route("/delete_data", methods=["GET"])
def delete_data():
    request_data = request.get_json()
    try:
        if get_role(request_data) == "admin":
            db.db.imdb_movie_data.delete_one(
                {
                    "99popularity": float(request_data["99popularity"]),
                    "director": request_data["director"],
                    "genre": request_data["genre"],
                    "imdb_score": float(request_data["imdb_score"]),
                    "name": request_data["name"],
                }
            )
            return "Data Deleted Successfully."
    except TypeError:
        return (
            jsonify(
                {
                    "success": True,
                    "data": "You don't have Access to perform this action.",
                }
            ),
            401,
        )


"""This API will help admin in editing the movie data by choosing the movie name"""


@app.route("/edit_data", methods=["GET"])
def edit_data():
    request_data = request.get_json()
    try:
        if get_role(request_data) == "admin":
            db.db.imdb_movie_data.update_one(
                {"name": request_data["value"]["name"]},
                {
                    "$set": {
                        "99popularity": float(request_data["99popularity"]),
                        "director": request_data["director"],
                        "genre": request_data["genre"],
                        "imdb_score": float(request_data["imdb_score"]),
                        "name": request_data["name"],
                    }
                },
            )
        return "Data Edited Successfully."
    except TypeError:
        return (
            jsonify(
                {
                    "success": True,
                    "data": "You don't have Access to perform this action.",
                }
            ),
            401,
        )


if __name__ == "__main__":

    app.run(host="0.0.0.0")
