Please follow app.py for code of all the APIs. 

Base URL : https://moviedata-app.herokuapp.com+ API NAME

Below is the list of API.

**User Access APIs**

1)  API NAME = /search_by_movie_name

Description = This API will work for complete and incomelete string both. You can search movies by movie name directly. 

request body = {"name":"psy"} or {"name":"psycho"}

Response : Movie data

2) API NAME = /search_by_director_name

Description = This API will work for complete and incomelete string both. You can search movies by director name. 

request body = {"director":"George"} or {"director":"George Lucas"}

Response : Movie data

3) API NAME = /search_for_greater_than_imdb_score

Description = This API will give the search feature based on IMDB score. The API will return only list of movies who will have greater IMDB score than the user entered value. 

request body = {"imdb_score":9}

Response : Movie data

4)  API NAME = /search_by_genre

Description = This API will give search based on genre. 

{
    "genre": [
                "Adventure",
                " Fantasy"
            ]
}

Response : Movie data


5) API NAME = /filter_search_imdb_value_and_genre

Description = This is the filter API which will allow user to set IMDB value and genre. 


request body =  {"imdb_score": 7,"genre": ["Crime"]}
                    
                 
Response : Movie data


**Admin Access APIs**

ID = 345 (Only admin with ID 345 is having access to the below APIs)

1) API NAME = /add_data

Description = This API will help admin in adding the movie data.

request body =  {
                    "user":{"user_id":345},
                    "99popularity":45,
                    "director":"xyz",
                    "genre":["xyz","xyz","xyz"],
                    "imdb_score":4.5,
                    "name":"Bhootnath"
                }


Response : "Data added" (for user with ID 345) for others "Not Authorised"


2) API NAME = /edit_data

Description = This API will help admin in editing the movie data by choosing the movie name.

request body =  {
                    "user":{"user_id":345},
                    "value":{"name":"Bhootnath"},
                    "99popularity":98,
                    "director":"abc",
                    "genre":["abc","abc","abc"],
                    "imdb_score":9.8,
                    "name":"Chichore"
                }

Response : "Data Edited" (for user with ID 345) for others "Not Authorised"

3) API NAME = /delete_data

Description = This API will help admin in deleting the movie data.

request body =  {
                    "user":{"user_id":345},
                    "99popularity":98,
                    "director":"abc",
                    "genre":["abc","abc","abc"],
                    "imdb_score":9.8,
                    "name":"Chichore"
                }

Response : "Data deleted" (for user with ID 345) for others "Not Authorised"










